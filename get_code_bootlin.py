import requests

import sys
import lxml

from lxml import etree
from io import StringIO, BytesIO
import os


import lxml.html


CLASS_FILE = "tree-icon icon-blob"
CLASS_FOLDER = "tree-icon icon-tree"

if len(sys.argv) == 1:
    print ("ej.")
    print ("  %s https://elixir.bootlin.com/linux/v4.19.100/source/drivers/i2c" % (sys.argv[0]))
    sys.exit()

else:
    if (sys.argv[1].find("https://elixir.bootlin.com/") != -1):
        print ("Getting files using %s like root ..." % sys.argv[0].replace("https://elixir.bootlin.com", "") )
    else:
        print("Just compatible for download files from Bootlin")
        print("Exiting ...")
        sys.exit()


def get_new_path_to_driver(url):

    driver_id = get_driver_id(url)
    so = get_so(url)
    kernel_version = driver_id.split("/")[1]
    driver_name = get_driver_name(url)
    complete_path = so + "/" + kernel_version + "/" + driver_name

    return complete_path


def get_driver_id(url):

    return url.replace("https://elixir.bootlin.com/", "")


def get_so(url):

    return get_driver_id(url).split("/")[0]


def get_kernel_version(url):

    return get_driver_id(url).split("/")[1]


def get_driver_name(url):

    return "".join(get_driver_id(url).split("/")[4:]).replace("/", "_")


def get_code_from_file(file):

    print (file)


def get_code_from_folder(folder):

    print (folder)


url = sys.argv[1]
driver_id = get_driver_id(url)
so = get_so(url)
kernel_version = driver_id.split("/")[1]
driver_name = get_driver_name(url)
complete_path = so + "/" + kernel_version + "/" + driver_name
req = requests.get(url)
main_text_html = req.text

# Parsing HTML

parser = etree.HTMLParser()
tree   = etree.parse(StringIO(main_text_html), parser)


folders = tree.findall(".//a[@class='%s']" % CLASS_FOLDER)
files = tree.findall(".//a[@class='%s']" % CLASS_FILE)

if len(folders) == 0 or len(files) == 0:
    print ("The folder is empty.")
    print ("Exiting.")
    sys.exit()
else:
    os.makedirs("./%s/%s/%s/" %(so, kernel_version, driver_name), exist_ok=True)

for file in files:
    get_code_from_file(file)

for folder in folders:
    get_code_from_folder(folder)

